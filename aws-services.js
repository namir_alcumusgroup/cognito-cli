const AWS = require('aws-sdk');
AWS.config.loadFromPath('./config.json');
const cognito = new AWS.CognitoIdentityServiceProvider();

const poolData = {
    UserPoolId: 'eu-west-2_qRQKJpzex',
    ClientId: 'kgt46g23mvae2cgrogis3dc8l'
};

const createUser = (userId, email, password, name) => {
    let createUserParams = {
        UserPoolId: poolData.UserPoolId, // From Cognito dashboard "Pool Id"
        Username: userId,
        MessageAction: 'SUPPRESS', // Do not send welcome email
        TemporaryPassword: password,
        UserAttributes: [
            {
                Name: 'email',
                Value: email
            },
            {
                // Don't verify email addresses
                Name: 'email_verified',
                Value: 'true'
            },
            {
                Name: 'name',
                Value: name
            },
        ]
    };
    return cognito.adminCreateUser(createUserParams).promise();
}

const initiateAuth = (userId, password) => {
    let initiateAuthParams = {
        AuthFlow: 'ADMIN_NO_SRP_AUTH',
        ClientId: poolData.ClientId, // From Cognito dashboard, generated app client id
        UserPoolId: poolData.UserPoolId,
        AuthParameters: {
            USERNAME: userId,
            PASSWORD: password
        }
    };
    return cognito.adminInitiateAuth(initiateAuthParams).promise();
}


const respondToAuth = (userId, password, session) => {
    let challengeResponseData = {
        USERNAME: userId,
        NEW_PASSWORD: password,
    };

    let newPasswordParams = {
        ChallengeName: 'NEW_PASSWORD_REQUIRED',
        ClientId: poolData.ClientId,
        UserPoolId: poolData.UserPoolId,
        ChallengeResponses: challengeResponseData,
        Session: session
    };
    return cognito.adminRespondToAuthChallenge(newPasswordParams).promise();
}

const confirmUserEmail = (username) => {
    let params = {
        Username: username,
        UserPoolId: poolData.UserPoolId
    }
    return cognito.adminConfirmSignUp(params).promise();
}

const deleteUser = (username) => {
    let params = {
        Username: username,
        UserPoolId: poolData.UserPoolId
    }
    return cognito.adminDeleteUser(params).promise();
}

//createCognitoUser('nabnab', 'bob@mail.com', 'Password1', '50 Cent')

module.exports = {
    respondToAuth,
    initiateAuth,
    createUser,
    confirmUserEmail,
    deleteUser
}
