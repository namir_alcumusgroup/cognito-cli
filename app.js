const awsService = require('./aws-services');

require('yargs')
    .usage('Usage: $0 <command> [options]')
    .command('confirmUser', 'confirm a user', (yargs) => {
        return yargs.option('username', {
            demand: 'Please specify a username'
        })
        }, async (argv) => {
            try {
                await awsService.confirmUserEmail(argv.username);
                console.log(`User ${argv.username} has been confirmed`);
            } catch (err) {
                console.error('Failed to confirm user');
                console.error(err);
            }
        }
    )
    .command('deleteUser', 'delete a user', (yargs) => {
        return yargs.option('username', {
            demand: 'Please specify a username'
        })
        }, async (argv) => {
            try {
                await awsService.deleteUser(argv.username);
                console.log(`User ${argv.username} has been removed`);
            } catch (err) {
                console.error('Failed to delete user');
                console.error(err);
            }
        }
    )
    .demandCommand()
    .argv;
